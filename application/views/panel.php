<section class="row">
    <article class="col-xs-3">
        <?= img('http://eventsbluedolphin.com.ve/images/img11.jpg','width:100%',FALSE); ?>
        <div class="list-group">
            <?php if($_SESSION['cuenta']==1): ?>
            <a class="list-group-item"><b>Admin</b></a>
            <a class="list-group-item <?= !empty($menu) && $menu=='usuarios'?'active':'' ?>" href="<?= base_url('panel/usuarios') ?>">Usuarios</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='fichas'?'active':'' ?>" href="<?= base_url('panel/fichas') ?>">Fichas de inscripción</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='pagos'?'active':'' ?>" href="<?= base_url('panel/pagos') ?>">Pagos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='itinerarios'?'active':'' ?>" href="<?= base_url('panel/itinerarios') ?>">Planes</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='productos'?'active':'' ?>" href="<?= base_url('panel/productos') ?>">Productos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='ventas'?'active':'' ?>" href="<?= base_url('panel/ventas') ?>">Ventas</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='bancos'?'active':'' ?>" href="<?= base_url('panel/bancos') ?>">Bancos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='ajustes'?'active':'' ?>" href="<?= base_url('panel/ajustes') ?>">Ajustes</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='grupos'?'active':'' ?>" href="<?= base_url('panel/grupos') ?>">Grupos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='cliente'?'active':'' ?>" href="<?= base_url('panel/clientes') ?>">Clientes</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='contratos'?'active':'' ?>" href="<?= base_url('panel/contratos') ?>">Contratos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='logos'?'active':'' ?>" href="<?= base_url('panel/logos') ?>">Logos de Clientes</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='facturas'?'active':'' ?>" href="<?= base_url('panel/facturas') ?>">Enviar facturas</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='servicios'?'active':'' ?>" href="<?= base_url('panel/servicios') ?>">Registrar servicios</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='combos'?'active':'' ?>" href="<?= base_url('panel/combos') ?>">Registrar combos</a>
            <?php endif ?>            
            <a class="list-group-item"><b><?= $_SESSION['nombre'] ?></b></a>            
            <a class="list-group-item <?= !empty($menu) && $menu=='fichas'?'active':'' ?>" href="<?= base_url('cliente/fichas') ?>">Fichas de inscripción</a>            
            <a class="list-group-item <?= !empty($menu) && $menu=='pagos'?'active':'' ?>" href="<?= base_url('cliente/pagos') ?>">Pagos</a>                       
            <a class="list-group-item <?= !empty($menu) && $menu=='inscribir'?'active':'' ?>" href="<?= base_url('cliente/itinerario') ?>">Inscribir</a>                       
            <a class="list-group-item <?= !empty($menu) && $menu=='compras'?'active':'' ?>" href="<?= base_url('cliente/ventas') ?>">Compras</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='logos'?'active':'' ?>" href="<?= base_url('cliente/logos') ?>">Nuestros clientes</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='solicitar_servicios'?'active':'' ?>" href="<?= base_url('cliente/servicios') ?>">¿Necesitas organizar un evento?</a>
            <a class="list-group-item"><b>Juridicos</b></a> 
            <a class="list-group-item <?= !empty($menu) && $menu=='empresa'?'active':'' ?>" href="<?= base_url('cliente/clientes') ?>">Mi empresa</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='contrato'?'active':'' ?>" href="<?= base_url('cliente/contratos') ?>">Mis contratos</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='facturas'?'active':'' ?>" href="<?= base_url('cliente/facturas') ?>">Mis facturas</a>
            <a class="list-group-item <?= !empty($menu) && $menu=='salir'?'active':'' ?>" href="<?= base_url('main/unlog') ?>">Salir</a>
        </div>
    </article>
    
        <article class="col-xs-8 col-xs-offset-1">
            <?php if(empty($crud)): ?>
                Contenido
            <?php else: ?>
                <?= $this->load->view('cruds/'.$crud);  ?>
            <?php endif ?>
        </article>
</section>
