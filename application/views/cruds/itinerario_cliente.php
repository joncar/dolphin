<?php 
if(!empty($nino)):    
$datos = $this->db->get_where('fichas',array('id'=>$nino,'user'=>$_SESSION['user']));
if($datos->num_rows>0):
$datos = $datos->row();

$this->db->where('grupos_itinerarios.grupo',$grupo);

$this->db->select('itinerarios.*');
$this->db->join('grupos_itinerarios','grupos_itinerarios.itinerario = itinerarios.id');
$itinerarios = $this->db->get('itinerarios');
?>
<form id="formulario" action="" onsubmit="return sendval();">    
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Seleccione los planes para el niño/a <b><?= $datos->nombres ?></b> <br/> Monto total de plan: <b><span class="monto">0</span> Bs.</b>
            </a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
                <div  class='row itinerarios' style="padding:10px;">
                    <?php foreach($itinerarios->result() as $i): ?> 
                    <?php //Verificar si ya lo compro 
                        $this->db->join('ventas','ventas.id = ventas_descripcion.venta');
                        $ventas = $this->db->get_where('ventas_descripcion',array('grupo'=>$grupo,'nino'=>$nino));
                        $ya = FALSE;
                        foreach($ventas->result() as $v){
                            $z = explode('_',$v->producto);
                            if($z[0] == $i->id)
                              $ya = TRUE;
                        }
                        if(!$ya):
                    ?>
                    <div class="btn-group col-xs-12" data-toggle="buttons" style="margin:20px;">
                        <label class="btn btn-primary">
                          <input name="itinerario_nombre[]" value="<?= $i->id ?>_Semana <?= date("d/m/Y",strtotime($i->inicio)) ?> al <?= date("d/m/Y",strtotime($i->final)) ?>" type="checkbox" autocomplete="off">
                          <input name="itinerario[]" value="<?= $i->precio ?>" class="itincheck" type="checkbox" autocomplete="off"> Semana <?= date("d/m/Y",strtotime($i->inicio)) ?> al <?= date("d/m/Y",strtotime($i->final)) ?>
                        </label>      
                    </div>
                    <div class="col-xs-12">
                        <?= $i->descripcion ?>
                        <div align="right"><b>Precio: <span style="color:red"><?= $i->precio ?></span></b></div>
                    </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
          </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Selecciona productos adicionales
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
              <div class="row">
                <?php foreach($this->db->get_where('productos')->result() as $p): ?>
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <?= img('files/'.$p->foto,'width:100%') ?>
                    <input type='hidden' name='producto_precio[]' value='<?= $p->precio ?>'>
                    <input type='hidden' name='producto[]' value='<?= $p->nombre ?>'>
                    <div class="caption">
                      <h3><?= $p->nombre ?></h3>
                      <p><?= $p->descripcion ?></p>
                      <?php if(!empty($p->tallas)): ?>
                      <?php $ta = explode(',',$p->tallas); ?>
                      <?php $tal = array(); foreach($ta as $x)$tal[$x] = $x; ?>
                      <p><b>Talla: </b><?php echo  form_dropdown('talla[]',$tal,0,'id="talla" class="form-control"') ?></p>
                      <?php else: ?>
                      <input type='hidden' name='talla[]'>
                      <?php endif ?>
                      <p align='right' style='color:red'><?= $p->precio ?> Bs</p>
                      <p><input type='number' data-precio='<?= $p->precio ?>' class='cantidad' name='cantidad_producto[]' value='0' readonly></p>
                      <p><a href="#" class="caradd btn btn-primary" role="button"><i class='fa fa-shopping-cart'></i> +</a> <a href="#" class="carminus btn btn-default" role="button"><i class='fa fa-shopping-cart'></i> -</a></p>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>                
              </div>
          </div>
        </div>
    </div>
</div>
    <div class="alert" style="display:none"></div>
<div class="row" align="right">
    <input type="hidden" name="total" id="total" value="0">
    <input type="hidden" name="categoria" id="total" value="<?= $categoria ?>">
    <input type="hidden" name="nino" id="nino" value="<?= $nino ?>">    
    <input type="hidden" name="grupo" id="grupo" value="<?= $grupo ?>">    
    <div align='right'> Monto total de itinerarios: <b><span class="monto">0</span> Bs.</b></div>
    <input type="hidden" name="id" id="id" value="<?= $nino ?>">
    <button type="submit" class="btn btn-success">Ir al siguiente paso</button>
</div>
</form>
<script>
    var total = 0;
    $(document).on('ready',function(){
        $(".itincheck").on('change',function(){
            $(window).trigger('total');
        });
        
        $(".caradd").on('click',function(e){            
            e.preventDefault();
            inp = $(this).parent().parent().find('.cantidad');            
            val = parseInt(inp.val());
            inp.val(val+=1);
            $(window).trigger('total');
        });
        
        $(".carminus").on('click',function(e){            
            e.preventDefault();
            inp = $(this).parent().parent().find('.cantidad');            
            val = parseInt(inp.val());
            inp.val(val>0?val-1:0);
            $(window).trigger('total');
        });
    });
    
    $(window).on('total',function(){ 
        total = 0;
        itin = 0;
        totalitin = $("body").find('.itinerarios .itincheck').length;
        $(".itinerarios .itincheck").each(function(){
            if($(this).prop('checked'))
                total += parseFloat($(this).val());            
            itin++;
            if(itin==totalitin){
                $(".cantidad").each(function(){                
                    total+=parseInt($(this).val())*parseFloat($(this).data('precio'));                
                    $(".monto").html(total);
                    $("#total").val(total);
                });
            }
        });
    })
    
    function sendval(){
        var data = document.getElementById('formulario');
        $(document).trigger('total')
        data = new FormData(data);
        $.ajax({
            url:'<?= base_url('json/itinerario') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data.status=='success'){
                    document.location.href="<?= base_url('cliente/ventas/edit/') ?>/"+data.id;
                }
                else
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data.message).show();
            }
            });
            return false;
    }
</script>
<?php else: ?>
<h1>Ha ocurrido un error</h1>
<?php endif ?>
<?php endif ?>