<?php $this->load->view('predesign/datepicker'); ?>
<script>
	var combo_servicio = [];
	$(document).ready(function(){
		$(".combo").click(function(){
			checked = $(this).prop('checked');
			targets = $(this).data('target');
			targets = targets.split(',');
			for(i in targets){
				$("#ser_"+targets[i]).prop('checked',checked);
				$("#ser_"+targets[i]).attr('disabled',checked);
			}
			$(".combo").attr('disabled',checked);
			$(this).attr('disabled',false);
		});
	});
</script>
<h1>
	Eventos	
</h1>
<p>
	Eventsbluedolphin te ayuda a organizar tu evento, solicita de manera online equipos, servicios o refrigerios para tus fiestas u eventos empresariales.
</p>
<form action="<?= base_url('cliente/solicitar_servicio') ?>" onsubmit="" method="post">
<div class="panel panel-default">
	<div class="panel-heading">Datos básicos</div>
	<div class="panel-body">
		<b>Su evento será el dia:</b>
		<p><input name="fecha" type="text" class="form-control datetime-input"></p>
		<b>¿Cuantas horas dura el evento?:</b>
		<p><input name="horas" type="number" class="form-control"></p>
		<b>¿Su evento es en caracas?</b>
		<p><input name="lugar" value="1" type="radio"> Si <input value="0" name="lugar" type="radio"> No</p>
		<b>¿En el lugar del evento existe un lugar para descarga?</b>
		<p><input name="descarga" value="1" type="radio"> Si <input name="descarga" value="0" type="radio"> No</p>
		<b>¿Para llegar al lugar del evento se debe subir escaleras?</b>
		<p><input name="escaleras" value="1" type="radio"> Si <input name="escaleras" value="0" type="radio"> No</p>
		<b>¿El lugar del evento cuenta con estacionamiento?</b>
		<p><input name="estacionamiento" value="1" type="radio"> Publico <input name="estacionamiento" value="0" type="radio"> Privado</p>
	</div>	
</div>
<div class="panel panel-default">
	<div class="panel-heading">Combos</div>
	<div class="panel-body">
		<div class="row">
		<?php foreach($this->db->get('combos')->result() as $c): ?>
			<?php 
				$servicios = '';
				foreach($this->db->get_where('combos_servicios',array('combo'=>$c->id))->result() as $s)
				$servicios.= $s->id.',';
			?>
			<div class="col-xs-4 col-sm-*">
				<input type="checkbox" data-target="<?= $servicios ?>" id="comb_<?= $c->id ?>" class="combo" name="combo[]" value="<?= $c->id ?>"> <?= $c->nombre ?></div>			
			
		<?php endforeach ?>
		</div>
	</div>	
</div>
<div class="panel panel-default">
	<div class="panel-heading">Adicionales</div>
	<div class="panel-body">
		<div class="row">
		<?php foreach($this->db->get('servicios')->result() as $c): ?>
			<div class="col-xs-4 col-sm-*"><input type="checkbox" name="servicios[]" id="ser_<?= $c->id ?>" value="<?= $c->id ?>"> <?= $c->descripcion ?></div>
		<?php endforeach ?>
		</div>
	</div>	
</div>
<p align="center"><button type="submit" class="btn btn-success">Ir al paso 2</button></p>
</form>
