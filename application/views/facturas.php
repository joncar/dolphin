<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
              <div class="col-sm-8" id="cliente_div">
                  <? $sel = empty($factura)?0:$factura->cliente; ?>
                  <?= form_dropdown_from_query('cliente','clientes','id','nombre',$sel,'id="cliente"') ?>
              </div>
            </div>
        </div> 
        <div class="col-xs-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Transacción: </label>
              <div class="col-sm-8">
                  <? $sel = empty($factura)?0:$factura->contado; ?>
                  <?= form_dropdown('contado',array('0'=>'Contado','1'=>'Credito'),0,'id="field-contado" class="form-control"') ?>
              </div>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($factura)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($factura->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Control: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="control" name="control" value="<?= empty($factura)?'':$factura->control; ?>">
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Factura: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="nro_factura" name="nro_factura" value="<?= empty($factura)?'':$factura->nro_factura; ?>">
              </div>
            </div>
        </div> 
    </div>    
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:20%">Cant.</th>
                        <th style="width:20%">Descripción</th>                        
                        <th style="width:20%">Precio Venta</th>
                        <th style="width:20%">Alicuota %</th>                                                
                        <th style="width:20%">Total</th>    
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php if(!empty($factura)): ?>
                        <?php foreach($detalles->result() as $d): ?>
                            <tr>
                                <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"  value="<?= $d->cantidad ?>"></td>
                                <td><input name="descripcion[]" type="text" class="form-control producto" placeholder="Descripcion" value="<?= $d->descripcion ?>"></td>                                
                                <td><input name='precio_venta[]' type="text" class="form-control precio_venta" value="<?= $d->precio_venta ?>" placeholder="Precio venta"></td>
                                <td><input name='alicuota[]' type="text" class="form-control alicuota" placeholder="Alicuota" value="<?= $d->alicuota ?>"></td>                                
                                <td><input name='total[]' type="text" class="form-control total" placeholder="Total" value="<?= $d->total ?>"></td>
                                <td><p align="center">
                                        <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                        <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                                    </p></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>
                        <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad" ></td>
                        <td><input name="descripcion[]" type="text" class="form-control producto" placeholder="Descripcion"></td>
                        <td><input name='precio_venta[]' type="text" class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='alicuota[]' type="text" class="form-control alicuota" placeholder="% Alicuota"></td>
                        <td><input name='total[]' type="text" class="form-control total" placeholder="Total"></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top:40px">
        <div class="col-xs-5">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Subtotal: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="subtotal" id="subtotal"  value="<?= empty($factura)?0:$factura->subtotal ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">IVA: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="iva" id="iva" value="<?= empty($factura)?0:$factura->iva ?>">
              </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-xs-5">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Total </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="total" id="total" value="<?= empty($factura)?0:$factura->total ?>">
              </div>
            </div>
        </div>
    </div> 
    <?php if($this->router->fetch_class()=='panel'): ?>
    <div class="row" style="margin-top:40px">        
        <a href="<?= base_url($this->router->fetch_class().'/clientes/add') ?>" target="_new" class="btn btn-default">Agregar Nuevo Cliente</a>                
        <button type="submit" class="btn btn-success">Guardar Compra</button>        
    </div>
    <?php else: ?>
    <div class="row" style="margin-top:40px">        
        <a target='_new' href="<?= base_url('cliente/imprimir_factura/'.$factura->id) ?>" target="_new" class="btn btn-default">Imprimir factura</a>        
    </div>
    <?php endif ?>
</form>
<?php if($this->router->fetch_class()=='panel'): ?>
<?php $this->load->view('predesign/datepicker') ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script>
    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }        
    
    //Eventos
    $(document).on('keydown','input',function(event){        
            
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    })

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    });                
    
    function val_send(form){
        var data = document.getElementById('formulario');        
        data = new FormData(data);
        $.ajax({
            url:'<?= empty($factura)?base_url('json/facturas/add'):base_url('json/facturas/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data['status']){
                    $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();
                    document.location.reload();
                }
                else
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
            }
            });
            return false;
    }
</script>
<?php endif ?>