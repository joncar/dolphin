<!Doctype html>
<html lang="es">
	<head>
		<title><?= empty($title)?'Recreaciones Dolphin':$title ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">                
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>                
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php endif; ?>                                
                <? if(empty($css_files) && empty($js_files)): ?><script src="http://code.jquery.com/jquery-1.9.0.js"></script><? endif ?>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true"></script>
                <script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
                <script src="<?= base_url('js/file_upload.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
                <link type="text/css" rel="stylesheet" href="http://eventsbluedolphin.com.ve/styles.css" />
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/font-awesome.css') ?>">                
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">                
	</head>
	<body>
            <section>
                <?= $this->load->view($view); ?>
            </section>
        </body>
</html>