<div class="alert" style="display:none"></div>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <?php if($_SESSION['cuenta']==1 && $this->router->fetch_class()=='panel'): ?><div class="col-xs-12"><a href="<?= base_url('panel/ventas_edit/edit/'.$venta->id) ?>" class="btn btn-default"><i class="fa fa-edit"></i> Editar venta</a></div><?php endif ?>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Categoria: </label>
              <div class="col-sm-8" id="cliente_div">
                  <h4><?
		  switch($venta->categoria){
			case '1': echo 'Plan Vacacional'; break;
			case '2': echo 'Campamento'; break;
			case '3': echo 'Contratación de servicios'; break;
		  } ?></h4>
			<input type="hidden" name="categoria" value="<?= $venta->categoria ?>">
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
              <div class="col-sm-8" id="cliente_div">
                  <?php $this->db->where('id',$venta->user) ?>
                  <?= form_dropdown_from_query('user','user','id','nombre',0,'id="user"') ?>
              </div>
            </div>
        </div>
	<?php if($venta->categoria!=3): ?>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Niño: </label>
              <div class="col-sm-8" id="cliente_div">
                  <?php $this->db->where('id',$venta->nino) ?>
                  <?= form_dropdown_from_query('ficha','fichas','id','nombres',0,'id=""') ?>
              </div>
            </div>
        </div>
	<?php endif ?>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" readonly value="<?= empty($venta)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime(str_replace("/","-",$venta->fecha))) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div> 
    </div>    
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th <?php if($_SESSION['cuenta']==1 && $this->router->fetch_class()=='panel')echo 'colspan="2"' ?> style="width:20%">Producto</th>
                        <th style="width:10%">Cantidad</th>                                                
                        <th style="width:10%">Precio</th>
                        <th style="width:10%">IVA</th>
                        <th style="width:10%">Descuento</th>                        
                        <th style="width:10%">Total</th>                            
                    </tr>
                </thead>
                <tbody> 
                    <?php if(!empty($venta)): ?>
			<?php $de = $venta->categoria!=3 || $this->router->fetch_method()=='ventas'?$detalles->result():$venta->detalles; ?>
                        <?php foreach($de as $d): ?>
                            <tr>
                                <?php if($_SESSION['cuenta']==1 && $this->router->fetch_class()=='panel'): ?><td><a href="<?= base_url('panel/ventas_edit_detalles/edit/'.$d->id) ?>" class="btn btn-default"><i class="fa fa-edit"></i></a></td><?php endif ?>
                                <?php $d->producto = explode("_",$d->producto); $d->producto = count($d->producto)>1?$d->producto[1]:$d->producto[0]; ?>
                                <td><input readonly name="producto[]" type="text" class="form-control producto" readonly placeholder="Nombre" value="<?= $d->producto ?>"></td>
                                <td><input readonly name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"  value="<?= $d->cantidad ?>"></td>
                                <td><input readonly name="monto[]" type="text" class="form-control precio" placeholder="Precio"  value="<?= $d->monto ?>"></td>
                                <td><input readonly name='iva[]' type="text" class="form-control iva" value="<?= $d->iva ?>" placeholder="IVA"></td>
                                <td><input readonly name='descuento[]' type="text" class="form-control descuento" placeholder="Descuento" value="<?= $d->descuento ?>"></td>
                                <td><input readonly name='total[]' type="text" class="form-control total" placeholder="Total" value="<?= $d->total ?>"></td>                                
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>                   
                </tbody>
            </table>
        </div>
    </div>
    <?php if($venta->categoria==3): ?>
    <p><b>Recargos adicionales</b></p>
    <div class="row">	
	<div class="col-xs-*">Recargo por estacionamiento: <?= $venta->recargo_estacionamiento==1?'Si':'No' ?> <input type="hidden" name="recargo_estacionamiento" value="<?= $venta->recargo_estacionamiento ?>"></div>
	<div class="col-xs-*">Recargo por zona de descarga: <?= $venta->recargo_descarga==1?'Si':'No' ?> <input type="hidden" name="recargo_descarga" value="<?= $venta->recargo_descarga ?>"></div>
	<div class="col-xs-*">Recargo por subir/bajar escaleras: <?= $venta->recargo_escaleras==1?'Si':'No' ?> <input type="hidden" name="recargo_escaleras" value="<?= $venta->recargo_escaleras ?>"></div>
	<div class="col-xs-*">Recargo nocturno: <?= $venta->recargo_nocturno==1?'Si':'No' ?> <input type="hidden" name="recargo_nocturno" value="<?= $venta->recargo_nocturno ?>"></div>
	<div class="col-xs-*">Lugar: <?= $venta->lugar==1?'Caracas':'Interior' ?> <input type="hidden" name="lugar" value="<?= $venta->lugar ?>"></div>
    </div>
    <?php endif ?>
    <div class="row" style="margin-top:40px">
        <div class="col-xs-5 col-xs-offset-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Deuda acumulada: </label>
              <div class="col-sm-8">
                  <input type="text" readonly class="form-control" name="deuda" id="total_dolares"  value="<?= empty($venta)?0:$venta->deuda ?>"> 
              </div>
            </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Total: </label>
              <div class="col-sm-8">
                  <input type="text" readonly class="form-control" name="monto_total" id="total_dolares"  value="<?= empty($venta)?0:$venta->monto_total ?>"> <?php if($this->router->fetch_method()=='ventas'): ?><a href="<?= base_url('cliente/pagos') ?>" class='btn btn-default'><i class='fa fa-money'></i> Ir al modulo de pagos</a><?php endif ?>
              </div>
            </div>
        </div>
    </div> 
    <div class="row" style="margin-top:40px">
        <div class="col-xs-12">
            <div class="form-group">              
                <div class="col-xs-12"><b>Bancos</b></div> 
              <div class="col-sm-12">
                  <?php foreach($this->db->get('bancos')->result() as $b): ?>
                  <div><b><?= $b->banco ?></b> cuenta <b><?= $b->tipo_cuenta ?></b> Nro. <b><?= $b->numero ?></b> a Nombre de: <b><?= $b->beneficiario ?></b></div>
                  <?php endforeach ?>
              </div>
            </div>
        </div>
    </div>
    <?php if($this->router->fetch_method()=='ventas'): ?>
    <div class="row" style="margin-top:40px">                            
        <div class="col-sm-12" align='center'>
            <a href="<?= base_url('cliente/imprimir_planilla/'.$venta->id) ?>" class="btn btn-default"><i class='fa fa-print'></i> Imprimir planilla</a>
        </div>
    </div>
    <?php else: ?>
    <div class="row" style="margin-top:40px">                            
        <div class="col-sm-12" align='center'>
            <button type="submit" class="btn btn-default">Guardar y solicitar</button>
        </div>
    </div>
    <?php endif ?>
</form>
<?php if($this->router->fetch_method()=='solicitar_servicio'): ?>
<script>
function val_send(form){
var data = document.getElementById('formulario');
data = new FormData(data);
$.ajax({
    url:'<?= base_url('json/ventas') ?>',
    method:'post',
    data:data,
    processData:false,
    cache: false,
    contentType: false,
    success:function(data){
        data = JSON.parse(data);
        if(data['status']){
            $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();
            document.location.href="<?= base_url('cliente/ventas') ?>";
        }
        else
            $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
    }
    });
    return false;
}
</script>
<?php endif ?>
