<?php $logo = $venta->categoria==0?'logo_campamento.png':'logo.png'; ?>
<page>
    <table>
        <tr>
            <td align='left' valign='top' width='350'><?= img('img/'.$logo,'width:30%') ?></td>
        </tr>
    </table>
    <?php        
        $this->db->order_by('producto','DESC');
        $this->db->join('ventas_descripcion','ventas_descripcion.venta = ventas.id');
        $ventas = $this->db->get_where('ventas',array('grupo'=>$venta->grupo,'nino'=>$ficha->id)) 
    ?>
    <?php foreach($ventas->result() as $v): ?>
    <?php 
        $p = explode('_',$v->producto);
        if(count($p)>1):        
        $i = $this->db->get_where('itinerarios',array('id'=>$p[0]))->row() 
                
                ?>
    <div align='center' style="margin-top:20px;">
        <b>Semana <?= date("d/m/Y",strtotime($i->inicio)) ?> al <?= date("d/m/Y",strtotime($i->final)) ?></b>
    </div>
    <div style="margin-top:20px;">
        <?= $i->descripcion ?>
    </div>
    <?php endif ?>
    <?php endforeach ?>
</page>
<page>
    <?= $normas ?>
</page>
<page> 
<table>
    <tr>
        <td align='left' valign='top' width='350'><?= img('img/'.$logo,'width:30%') ?></td>
        <td align='right' valign='top'  width='350'><?= img('files/'.$ficha->foto,'width:30%') ?></td>
    </tr>
</table>
<div align='center' style="margin-top:40px;">
    <b>PLANILLA DE INSCRIPCIÓN PLAN VACACIONAL TEMPORADA <?= date("Y"); ?></b>
</div>
<br/><br/>
<table>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DATOS DEL NIÑO</td>
    </tr>
    <tr>
        <td>Apellido(S) y Nombre(s):</td>
        <td style='border-bottom: 1px solid black;'> <?= $ficha->nombres.' '.$ficha->apellidos ?></td>
    </tr>
    <tr>
        <td>Sexo (Indique con una X): </td>
        <td>M: ____<?= $ficha->sexo=='M'?'X':'' ?>____ F: ____<?= $ficha->sexo=='F'?'X':'' ?>____</td>
    </tr>
    <tr>
        <td>Fecha de nacimiento (dd/mm/aa): </td>
        <td><?= date("d/m/Y",strtotime($ficha->fecha_nacimiento)) ?> Edad: <?= $ficha->edad ?></td>
    </tr>
    
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DIRECCIÓN DE HABITACIÓN</td>
    </tr>
    <tr>
        <td>Estado: </td>
        <td><u><?= $ficha->estado ?></u> Ciudad: <u><?= $ficha->ciudad ?></u></td>
    </tr>
    <tr>
        <td>Urb Av. Calle: </td>
        <td><u><?= $ficha->direccion ?></u></td>
    </tr>
    <tr>
        <td>Nombre del Inmueble: </td>
        <td><u><?= $ficha->casa ?></u> Piso: <u><?= $ficha->piso ?></u> Nro: <u><?= $ficha->apt ?></u></td>
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DATOS DEL REPRESENTANTE</td>
    </tr>
    <tr>
        <td>Apellido(S) y Nombre (s): </td>
        <td><u><?= $ficha->nombre_representante.' '.$ficha->apellido_representante ?></u></td>
    </tr>
     <tr>
        <td>Parentesco: </td>
        <td><u><?= $ficha->parentesco ?></u></td>
    </tr>
    <tr>
        <td>Ocupación: </td>
        <td><u><?= $ficha->ocupacion ?></u></td>
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DIRECCIÓN DE HABITACIÓN</td>
    </tr>
    <tr>
        <td>Estado: </td>
        <td><u><?= $ficha->estado_rep ?></u> Ciudad: <u><?= $ficha->ciudad_rep ?></u></td>
    </tr>
    <tr>
        <td>Urb. Av. Calle: </td>
        <td><u><?= $ficha->direccion ?></u></td>
    </tr>
    <tr>
        <td>Nombre del inmueble: </td>
        <td><u><?= $ficha->casa_rep ?></u> Piso: <u><?= $ficha->piso_rep ?></u> Nro: <u><?= $ficha->apt_rep ?></u></td>
    </tr>
    <tr>
        <td>Teléfono Hab: </td>
        <td><u><?= $ficha->telefono_rep ?></u> Celular: <u><?= $ficha->celular_rep ?></u></td>
    </tr>
    <tr>
        <td>Teléfono de Oficina: </td>
        <td><u><?= $ficha->telefono2_rep ?></u> Otro Telefono: <u><?= $ficha->otro_telefono_rep ?></u></td>
    </tr>
    <tr>
        <td>Correo electrónico: </td>
        <td><u><?= $ficha->email_rep ?></u></td>
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DATOS DEL FAMILIAR Y/O PERSONA CONTACTO</td>
    </tr>
    <tr>
        <td>Apellido(S) y Nombre(s): </td>
        <td><u><?= $ficha->nombre_fam.' '.$ficha->apellido_fam ?></u></td>
    </tr>
    <tr>
        <td>Parentesco: </td>
        <td><u><?= $ficha->parentesco_fam ?></u></td>
    </tr>
    <tr>
        <td>Ocupación: </td>
        <td><u><?= $ficha->ocupacion_fam ?></u></td>
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>DIRECCIÓN DE HABITACIÓN</td>
    </tr>
    <tr>
        <td>Estado: </td>
        <td><u><?= $ficha->estado_fam ?></u> Ciudad: <u><?= $ficha->ciudad_fam ?></u></td>
    </tr>
    <tr>
        <td>Urb. Av. Calle: </td>
        <td><u><?= $ficha->direccion_fam ?></u></td>
    </tr>
    <tr>
        <td>Nombre del inmueble: </td>
        <td><u><?= $ficha->casa_fam ?></u> Piso: <u><?= $ficha->piso_fam ?></u> Nro: <u><?= $ficha->apt_fam ?></u></td>
    </tr>
    <tr>
        <td>Teléfono Hab: </td>
        <td><u><?= $ficha->telefono_fam ?></u> Celular: <u><?= $ficha->celular_fam ?></u></td>
    </tr>
    <tr>
        <td>Teléfono de Oficina: </td>
        <td><u><?= $ficha->telefono2_fam ?></u> Otro Telefono: <u><?= $ficha->otro_telefono_fam ?></u></td>
    </tr>
    <tr>
        <td>Correo electrónico: </td>
        <td><u><?= $ficha->email_fam ?></u></td>
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>INFORMACIÓN DEL NIÑO</td>
    </tr>
    <tr>
        <td colspan='2'>Indicar si el niño padece algúna enfermedad: </td>        
    </tr>
    <tr>
        <td colspan='2'><u><?= $ficha->enfermedad ?></u> </td>        
    </tr>
    <tr>
        <td colspan='2'>Indique si el niño es alergico algún alimento o medicamento: </td>        
    </tr>
    <tr>
        <td colspan='2'><u><?= $ficha->alergico ?></u> </td>        
    </tr>
    <tr>
        <td colspan='2'>Alguna información o dato especial que considere debe ser de nuestro interes en relación al niño o representado: </td>        
    </tr>
    <tr>
        <td colspan='2'><u><?= $ficha->informacion ?></u> </td>        
    </tr>
    <tr style="background:#333; color:white">
        <td width="700" colspan='2'>INFORMACIÓN DE INTERÉS A RECREATIONS DOLPHIN.</td>
    </tr>
    <tr>
        <td colspan='2'>Indiquenos que espera del Plan Vacacional para con su hijo o representado: </td>        
    </tr>
    <tr>
        <td colspan='2'><u><?= $ficha->espectativas ?></u> </td>        
    </tr>
</table>
<div align='center' style='border-top:1px solid black; width:300px; margin-top:40px;'>Firma y cédula de identidad del Padre y/o Representante.</div>
<div align='center' style='border-top:1px solid black; width:300px; margin-top:20px; margin-left: 400px;'>Coordinación de Planes y Campamentos Vacacionales <br/> J-31412425-0</div>
</page>