<div class="row">
<?php foreach($this->db->get('logos')->result() as $l): ?>
  <div class="col-xs-6 col-md-3">
    <a href="<?= $l->enlace ?>" class="thumbnail">
      <img title='<?= $l->nombre ?>' src="<?= base_url('files/'.$l->logo) ?>" alt="<?= $l->nombre ?>">
    </a>
  </div>
<?php endforeach ?>
</div>
