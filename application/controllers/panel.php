<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));    
                $cliente = $this->db->get_where('clientes',array('user'=>$_SESSION['user']));
                if($cliente->num_rows>0)
                   $_SESSION['cliente'] = $cliente->row()->id;
	}
       
        public function index($url = 'main',$page = 0)
	{		
                $this->loadView('panel');
	}                
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        }
        
        function usuarios($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','email','password','status','cuenta');  
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');            
            $crud->set_rules('password','Contraseña','required|min_length[8]');                                
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('cuenta','true_false',array('0'=>'Usuario','1'=>'Admin'));            
            //Callbacks            
            $crud->callback_before_insert(array($this,'usuario_binsertion'));
            $crud->callback_after_insert(array($this,'usuario_ainsertion'));
            $crud->callback_before_update(array($this,'usuario_binsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de clientes';
            $output->menu = 'usuarios';
            $this->loadView($output);
        }
        
        function bancos(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('bancos');
            $crud->set_subject('banco');
            $crud->required_fields('nombre','tipo_cuenta','numero','banco','beneficiario');
            $crud->field_type('tipo_cuenta','dropdown',array('Ahorro'=>'Ahorro','Corriente'=>'Corriente'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de bancos';
            $output->menu = 'bancos';
            $this->loadView($output);
        }
        
        function fichas(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('fichas');
            $crud->set_subject('Ficha de inscripcion');     
            if($this->router->fetch_class()=='panel')$crud->set_relation('user','user','nombre');
            $crud->set_field_upload('foto','files');            
            $crud->field_type('sexo','dropdown',array('M'=>'Masculino','F'=>'Femenino'));            
            $crud->field_type('fecha_registro','hidden',date("Y-m-d"));
            
            $crud->display_as('casa','Casa/Res');
            $crud->display_as('casa_fam','Casa/Res');
            $crud->display_as('casa_rep','Casa/Res');
            $crud->display_as('piso_rep','Piso');
            $crud->display_as('piso_fam','Piso');
            $crud->display_as('apt','Apartamento/Nro. Casa');
            $crud->display_as('apt_fam','Apartamento/Nro. Casa');
            $crud->display_as('apt_rep','Apartamento/Nro. Casa');
            $crud->display_as('estado_rep','Estado');
            $crud->display_as('estado_fam','Estado');
            $crud->display_as('ciudad_rep','Ciudad');
            $crud->display_as('ciudad_fam','Ciudad');
            $crud->display_as('direccion','Dirección');
            $crud->display_as('direccion_rep','Dirección');
            $crud->display_as('direccion_fam','Dirección');
            $crud->display_as('telefono','Teléfono/Hab');
            $crud->display_as('telefono_fam','Teléfono/Hab');
            $crud->display_as('telefono_rep','Teléfono/Hab');
            $crud->display_as('telefono2','Teléfono/Ofi');
            $crud->display_as('telefono2_rep','Teléfono/Ofi');
            $crud->display_as('telefono2_fam','Teléfono/Ofi');            
            $crud->display_as('celular_rep','Celular');
            $crud->display_as('celular_fam','Celular');
            $crud->display_as('parentesco_fam','Parentesco');
            $crud->display_as('parentesco_rep','Parentesco');
            $crud->display_as('ocupacion','Ocupación');
            $crud->display_as('ocupacion_fam','Ocupación');
            $crud->display_as('ocupacion_rep','Ocupación');            
            $crud->display_as('email_rep','Email');
            $crud->display_as('email_fam','Email');            
            $crud->display_as('otro_telefono_rep','Otro telefono');
            $crud->display_as('otro_telefono_fam','Otro telefono');
            $crud->display_as('nombre_fam','Nombres');
            $crud->display_as('apellido_fam','Apellidos');
            $crud->display_as('alergico','Alérgico');
            $crud->display_as('informacion','Información');
            $crud->display_as('espectativas','Expectativas');
            
            $crud->required_fields('user','foto','nombres','apellidos','sexo','fecha_nacimiento',
                    'edad','estado','ciudad','direccion','casa','piso','apt','nombre_representante','apellido_representante','parentesco','ocupacion',
                    'parentesco_rep','ocupacion_rep','estado_rep','ciudad_rep','direccion_rep','casa_rep','apt_rep','telefono_rep',
                    'celular_rep','nombre_fam','apellido_fam','parentesco_fam',
                    'ocupacion_fam','estado_fam','ciudad_fam','direccion_fam','casa_fam','piso_fam','apt_fam','telefono_fam',
                    'celular_fam','fecha_registro');
            
            $crud->columns('user','foto','nombres','apellidos');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks         
            if($this->router->fetch_class()=='panel'){
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'ficha';
            $output->title= 'Ficha de inscripción';
            $output->menu = 'fichas';
            $this->loadView($output);
            }
            else return $crud;
        }
        
        function clientes(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes');
            $crud->set_subject('Cliente');  
            $crud->set_field_upload('logo','files');
            
            
            $requireds = array();
            foreach($this->db->field_data('clientes') as $row)
                if($row->name!='url')array_push($requireds,$row->name);
            $crud->required_fields_array($requireds);
            $crud->columns('logo','nombre','rif','telefonos','email');
            $crud->display_as('direccion_fiscal','Dirección Fiscal');
            $crud->display_as('telefono_contacto','Teléfono Contacto');
            if($this->router->fetch_class()=='panel'){
            $crud->set_relation('user','user','nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de clientes';
            $output->menu = 'cliente';
            $this->loadView($output);       
            }else return $crud;
        }
        
        function contratos(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('contratos');
            $crud->set_subject('Contrato'); 
            $crud->set_relation_n_n('itinerarios','clientes_itinerarios','itinerarios','contrato','itinerario','{inicio} a {final}','priority');
            $crud->set_relation_n_n('integrantes','clientes_ninos','fichas','contrato','nino','{nombres} {apellidos}','priority');
            $crud->set_relation_n_n('adicionales','clientes_adicionales','productos','contrato','producto','nombre','priority',null,array('cantidad'));
            /*$requireds = array();
            foreach($this->db->field_data('contratos') as $row)
                array_push($requireds,$row->name);
            $crud->required_fields_array($requireds);*/
            
            $crud->set_relation('cliente','clientes','nombre');
            $crud->field_type('fecha','hidden',date("Y-m-d"));
            if($this->router->fetch_class()=='panel'){
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de contratos';
            $output->menu = 'contratos';
            $this->loadView($output);   
            }else return $crud;
        }
        
        function itinerarios($x = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('itinerarios');
            $crud->set_subject('Plan');               
            //Fields            
            $crud->field_type('categoria','dropdown',array('Campamento','Recreacion'));
            //unsets            
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields('inicio','final','precio','descripcion','categoria');            
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'itinerarios';
            $output->title= 'Registro de itinerarios';
            $output->menu = 'itinerario';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }
        
        function productos($x = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('productos');
            $crud->set_subject('Productos');               
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','precio');
            $crud->set_field_upload('foto','files');
            $crud->field_type('tallas','set',array('8','12','16','S','M','L','XL'));
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Productos';
            $output->menu = 'productos';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }
        
        function pagos($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('pagos');
            $crud->set_subject('Pagos');    
            $crud->unset_delete();
            $crud->set_relation('venta','ventas','id');
            $crud->set_relation('banco','bancos','{banco} {numero}');
            if($this->router->fetch_class()=='panel')$crud->set_relation('user','user','nombre');
            $crud->field_type('tipo_pago','enum',array('Deposito','Transferencia'));
            $crud->field_type('aprobado','dropdown',array('-1'=>'Rechazado',0=>'En revision',1=>'Aprobado'));
            $crud->set_field_upload('comprobante_foto','files');            
            $crud->callback_after_update(function($post,$id){
                if($post['aprobado']==1){
                    $deuda = $this->db->get_where('ventas',array('id'=>$post['venta']))->row()->deuda;
                    $deuda-=$post['monto'];
                    $deuda = $deuda<0?0:$deuda;
                    $this->db->update('ventas',array('deuda'=>$deuda),array('id'=>$post['venta']));
                }
            });
            if(!empty($_POST) && (empty($y) || !empty($y) && is_numeric($y) && $this->db->get_where('pagos',array('id'=>$y))->row()->comprobante!=$_POST['comprobante']))
            $crud->set_rules('comprobante','Comprobante','required|is_unique[pagos.comprobante]');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                          
            //Callbacks            
            $crud->required_fields('venta','monto','fecha','comprobante','tipo_pago','banco');
            if($this->router->fetch_class()=='panel'){
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de pagos';
            $output->menu = 'pagos';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
            }else return $crud;
        }
        
        function ventas($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ventas');
            $crud->set_subject('Ventas');             
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_read()
                 ->unset_print()
                 ->unset_export();
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations
            $crud->set_relation('nino','fichas','{nombres} {apellidos}');
            $crud->columns('id','nino','fecha','monto_total','deuda');
            $crud->display_as('id','Nro. Venta');
            //Callbacks                    
            if($this->router->fetch_class()=='panel'){
            //Callbacks                    
            $output = $crud->render();
            if($x=='add'){
                $output->output = $this->load->view('ventas',array(),TRUE);
            }
            if($x=='edit' && !empty($y) && is_numeric($y)){
                $venta = $this->db->get_where('ventas',array('id'=>$y));
                $detalles = $this->db->get_where('ventas_descripcion',array('venta'=>$venta->row()->id));
                $output->output = $this->load->view('ventas',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                        
            }
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de ventas';
            $output->menu = 'ventas';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
            }else return $crud;
        }
        
        function ventas_edit($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ventas');
            $crud->set_subject('Ventas');
            $crud->set_relation('user','user','nombre');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                        
            //Callbacks                                
            //Callbacks                    
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de ventas';
            $output->menu = 'ventas';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);            
        }
        
        function ventas_edit_detalles($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ventas_descripcion');
            $crud->set_subject('Ventas');
            $crud->set_relation('venta','ventas','id');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                        
            //Callbacks                                
            //Callbacks                    
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de ventas';
            $output->menu = 'ventas';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);            
        }
        
        function ajustes(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ajustes');
            $crud->set_subject('Ajuste');            
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                        
            //Callbacks                                
            //Callbacks                    
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Ajustes varios';
            $output->menu = 'ajustes';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);            
        }
        
        function grupos(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('grupos');
            $crud->set_subject('Grupo'); 
            $crud->required_fields('nombre','categoria');
            $crud->field_type('categoria','dropdown',array('Campamento','Recreacion'));
            $crud->set_relation_n_n('integrantes','grupos_ninos','fichas','grupo','ficha','{nombres} {apellidos}','priority');
            $crud->set_relation_n_n('itinerarios','grupos_itinerarios','itinerarios','grupo','itinerario','{inicio} a {final}','priority');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                        
            //Callbacks                                
            //Callbacks    
            $crud->columns('nombre','categoria','integrantes');
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Grupos';
            $output->menu = 'grupos';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);            
        }
        
        function logos(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('logos');
            $crud->set_subject('Logos'); 
            $crud->required_fields('nombre','logo');    
            $crud->set_field_upload('logo','files');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations                        
            //Callbacks                                
            //Callbacks                
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Grupos';
            $output->menu = 'logos';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);            
        }
        
        function facturas($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('facturas');
            $crud->set_subject('Factura');  
            $crud->where('cliente',$_SESSION['cliente']);
            //Fields
            $crud->columns('nro_factura');
            //unsets
            //Displays
            //Fields types
            //Validations
            //Callbacks
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Facturas';
            $output->menu = 'facturas';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            
            if($x=='add')
                $output->output = $this->load->view('facturas',array(),TRUE);                        
            if($x=='edit' && !empty($y) && is_numeric($y)){
                $venta = $this->db->get_where('facturas',array('id'=>$y));
                $detalles = $this->db->get_where('facturas_detalles',array('factura'=>$venta->row()->id));
                $output->output = $this->load->view('facturas',array('factura'=>$venta->row(),'detalles'=>$detalles),TRUE);                        
            }
            $this->loadView($output);
        }

	function servicios(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('servicios');
            $crud->set_subject('Servicio o equipos');
	    $crud->field_type('tipo','dropdown',array('1'=>'Servicio','2'=>'Refrigerio','3'=>'Equipo'));
            $crud->required_fields('descripcion','costo_hora_diurno_caracas','tipo','costo_porcion','costo_hora_nocturno_caracas','costo_hora_diurno_interior','costo_hora_nocturno_interior','costo_transporte_caracas','costo_transporte_interior','costo_adicional_descarga','costo_adicional_escaleras','costo_adicional_estacionamiento');
            $crud->field_type('tipo_cuenta','dropdown',array('Ahorro'=>'Ahorro','Corriente'=>'Corriente'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Servicios y equipos';
            $output->menu = 'servicios';
            $this->loadView($output);
        }

	function combos(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('combos');
            $crud->set_subject('Combos de servicios o equipos');
            $crud->required_fields('nombre','horas','costo_hora');
            $crud->field_type('tipo_cuenta','dropdown',array('Ahorro'=>'Ahorro','Corriente'=>'Corriente'));
	    $crud->set_relation_n_n('servicios','combos_servicios','servicios','combo','servicio','{descripcion}','priority');
	    $crud->display_as('servicios','Servicios incluidos');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de Servicios y equipos';
            $output->menu = 'combos';
            $this->loadView($output);
        }
        
        function usuario_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary))
                $post['password'] = md5($post['password']);
            return $post;
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
