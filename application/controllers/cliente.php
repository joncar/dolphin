<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Cliente extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{		
                parent::index();
	}
        
        function fichas(){
            $crud = parent::fichas();
            $crud->unset_delete();
            $crud->where('user',$_SESSION['user']);
            $crud->field_type('user','hidden',$_SESSION['user']);
            $crud->set_lang_string('insert_success_message','La ficha ha sido creada, <script>setTimeout(function(){document.location.href="'.base_url('cliente/itinerario').'"},2000)</script>');
            $crud->callback_after_insert(function($post,$id){
                $_SESSION['nino'] = $id;
                return true;
            });
            $crud->columns('foto','nombres','apellidos');
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'ficha';
            $output->title= 'Ficha de inscripción';
            $output->menu = 'fichas';
            $this->loadView($output);
        }
        
        function pagos($x = ''){
            $crud = parent::pagos();
            $crud->set_relation('venta','ventas','{id}-{deuda}',array('deuda >'=>0,'user'=>$_SESSION['user']));            
            $crud->where('pagos.user',$_SESSION['user']);
            $crud->field_type('user','hidden',$_SESSION['user']);
            $crud->unset_columns('user');
            $crud->unset_fields('aprobado');
            $crud->display_as('venta','Numero de compra');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de pagos';
            $output->menu = 'pagos';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }
        
        function ventas($x = '',$y = ''){
            $crud = parent::ventas($x,$y);            
            $crud->where('ventas.user',$_SESSION['user']);            
            $output = $crud->render();
            if($x=='add'){
                $output->output = $this->load->view('ventas',array(),TRUE);
            }
            if($x=='edit' && !empty($y) && is_numeric($y)){
                $venta = $this->db->get_where('ventas',array('id'=>$y));
                $detalles = $this->db->get_where('ventas_descripcion',array('venta'=>$venta->row()->id));
                $output->output = $this->load->view('ventas',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                        
            }
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de pagos';
            $output->menu = 'compras';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }  
        
        function itinerario($categoria = '',$id = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('grupos');
            $crud->set_subject('grupo'); 
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();
            $crud->field_type('categoria','dropdown',array('Campamento','Recreacion'));
            $crud->callback_column('categoria',function($val,$row){
                $val = $val==1?'Recreación':'Campamento';
                return '<a href="'.base_url('cliente/comprar_itinerario/'.$row->id.'/'.$row->categoria).'">'.$val.'</a>';
            });
            $output = $crud->render();            
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Inscribir niño';
            $output->menu = 'inscribir';     
            $output->output = '<h3>Selecciona un paquete</h3>'.$output->output;
            $this->loadView($output);
        }
        
        function comprar_itinerario($id,$categoria,$nino = ''){
            if(empty($nino)){
                $_SESSION['id'] = $id;
                $_SESSION['categoria'] = $categoria;
                $crud = parent::fichas();
                $crud->where('user',$_SESSION['user']);
                $crud->unset_add()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();
                $crud->columns('foto','nombres','apellidos');
                $crud->callback_column('nombres',function($val,$row){
                    return '<a href="'.base_url('cliente/comprar_itinerario/'.$_SESSION['id'].'/'.$_SESSION['categoria'].'/'.$row->id).'">'.$val.'</a>';
                });
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'ficha';
                $output->title= 'Selecciona un niño';                
                $output->output = '<h3>Selecciona un niño</h3>'.$output->output;
                $this->loadView($output);
            }
            else{
                $this->loadView(array('view'=>'panel','crud'=>'itinerario_cliente','grupo'=>$id,'categoria'=>$categoria,'nino'=>$nino));
            }
        }
        
        function imprimir_planilla($id = ''){
            if(!empty($id) && is_numeric($id) && $id>0){
                $venta = $this->db->get_where('ventas',array('id'=>$id,'user'=>$_SESSION['user']));
                if($venta->num_rows>0){
                    $venta = $venta->row();
                    $venta_detalle = $this->db->get_where('ventas_descripcion',array('venta'=>$venta->id));
                    $itinerarios = array();
                    foreach($venta_detalle->result() as $d){
                        $iti = explode("_",$d->producto);
                        if(count($iti)>1)
                            $itinerarios[] = $iti[0];
                    }
                    foreach($itinerarios as $i)
                        $this->db->or_where('id',$i);
                    $itinerarios = $this->db->get('itinerarios');
                    $ficha = $this->db->get_where('fichas',array('id'=>$venta->nino));
                    
                    if($venta_detalle->num_rows>0 && $itinerarios->num_rows>0  && $ficha->num_rows>0){
                        $ficha = $ficha->row();
                        $normas = $this->db->get_where('ajustes',array('id'=>1))->row()->contenido;
                        $contenido = $this->load->view('reportes/planilla',array('normas'=>$normas,'venta'=>$venta,'normas'=>$normas,'venta_detalle'=>$venta_detalle,'itinerarios'=>$itinerarios,'ficha'=>$ficha),TRUE);
                        
                        $this->load->library('html2pdf/html2pdf');
                        $html2pdf = new HTML2PDF('P','L','es',true,'UTF-8');
                        $html2pdf->writeHTML($contenido);
                        $html2pdf->Output('planilla'.'.pdf');
                    }
                    else header("Location:".base_url('panel'));                    
                }
                else header("Location:".base_url('panel'));
            }
            else header("Location:".base_url('panel'));
        }
        
        function clientes($x = '',$y = ''){
            $crud = parent::clientes($x,$y);
            $crud->set_subject('empresa');
            $crud->unset_delete()                 
                 ->unset_print()
                 ->unset_export();
            $crud->field_type('user','hidden',$_SESSION['user']);
            if($this->db->get_where('clientes',array('user'=>$_SESSION['user']))->num_rows>0)
                $crud->unset_add();
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de empresas';
            $output->menu = 'empresa';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }
        function contratos($x = '',$y = ''){
            $crud = parent::contratos($x,$y);
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_export();
            $crud->columns('cliente','codigo','fecha');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de contratos';
            $output->menu = 'contrato';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
            $this->loadView($output);
        }
        
        function facturas($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('facturas');
            $crud->set_subject('Factura');                         
            //Fields
            //unsets
            //Displays
            //Fields types
            //Validations
            //Callbacks
            //Callbacks
            $crud->columns('nro_factura');
            $crud->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_delete()
                 ->unset_read();
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Facturas';
            $output->menu = 'facturas';
            $output->edit = !empty($x) && $x=='edit'?TRUE:FALSE;
                        
            if($x=='edit' && !empty($y) && is_numeric($y)){
                $venta = $this->db->get_where('facturas',array('id'=>$y));
                $detalles = $this->db->get_where('facturas_detalles',array('factura'=>$venta->row()->id));
                $output->output = $this->load->view('facturas',array('factura'=>$venta->row(),'detalles'=>$detalles),TRUE);                        
            }
            $this->loadView($output);
        }
        
        function imprimir_factura($id = ''){
            if(!empty($id) && is_numeric($id) && $id>0){
                $venta = $this->db->get_where('facturas',array('id'=>$id));
                if($venta->num_rows>0){
                    $venta = $venta->row();
                    $venta_detalle = $this->db->get_where('facturas_detalles',array('factura'=>$venta->id));
                    
                    if($venta_detalle->num_rows>0){                       
                        $contenido = $this->load->view('reportes/facturas',array('factura'=>$venta,'venta_detalle'=>$venta_detalle),TRUE);
                        
                        $this->load->library('html2pdf/html2pdf');
                        $html2pdf = new HTML2PDF('P','L','es',true,'UTF-8');
                        $html2pdf->writeHTML($contenido);
                        $html2pdf->Output('planilla'.'.pdf');
                    }
                    else header("Location:".base_url('panel'));                    
                }
                else header("Location:".base_url('panel'));
            }
            else header("Location:".base_url('panel'));
        }

	function servicios(){
            $this->loadView(array('view'=>'panel','crud'=>'servicios','menu'=>'solicitar_servicio','title'=>'Solicitar Servicios'));
        }

	function solicitar_servicio($x = '',$y = ''){
            $this->form_validation->set_rules('fecha','Fecha','required');
	    if($this->form_validation->run()){
		$venta = array();
		$venta['categoria'] = 3;
		$venta['user'] = $_SESSION['user'];
		$venta['fecha'] = $_POST['fecha'];
		$venta['detalles'] = array();
		$venta['deuda'] = 0;
		if(!empty($_POST['combo'])){
			$this->db->select('combos.*');
			$this->db->join('combos_servicios','combos_servicios.combo = combos.id');
			$this->db->join('servicios','servicios.id = combos_servicios.servicio');
			$combo = $this->db->get_where('combos',array('combo'=>$_POST['combo'][0]))->row();
			$costo = $combo->costo_hora;
			$iva = $costo*0.12;
			$total = ($costo*$_POST['horas'])+$iva;
			$venta['detalles'][] = (object)array('producto'=>'Contratación del combo '.$combo->nombre,'cantidad'=>$_POST['horas'],'monto'=>$costo,'iva'=>$iva,'descuento'=>0,'total'=>$total);			
			$venta['deuda']+=$total;
		}

		if(!empty($_POST['servicios'])){
			$total = 0;
			foreach($_POST['servicios'] as $s){
				$this->db->select('servicios.*');			
				$combo = $this->db->get_where('servicios',array('id'=>$s))->row();
				//Buscamos si es nocturno o diurno y el lugar
				$lugar = !empty($_POST['lugar'])?'caracas':'interior';
				$horario = date("H",strtotime(str_replace("/","-",$_POST['fecha'])));
				$horario = $horario<8 || $horario >18?'nocturno':'diurno';
				$campo = 'costo_hora_'.$horario.'_'.$lugar;
				$monto = $combo->$campo*$_POST['horas'];
				$campo = 'costo_transporte_'.$lugar;
				$costo = $combo->$campo;
				if(!empty($_POST['descarga']))$costo+=$combo->costo_adicional_descarga;
				if(!empty($_POST['escaleras']))$costo+=$combo->costo_adicional_escaleras;
				if(!empty($_POST['estacionamiento']))$costo+=$combo->costo_adicional_estacionamiento;								
				$iva = $monto*0.12;
				$total = $monto+$costo+$iva;
				$venta['detalles'][] = (object)array('producto'=>$combo->descripcion,'cantidad'=>$_POST['horas'],'monto'=>$monto,'iva'=>$iva,'descuento'=>0,'total'=>$total);			
				$venta['deuda']+=$total;
			}
		}		
		$venta['monto_total'] = $venta['deuda'];
		$venta['recargo_estacionamiento'] = $_POST['estacionamiento'];
		$venta['recargo_descarga'] = $_POST['descarga'];
		$venta['recargo_escaleras'] = $_POST['escaleras'];
		$horario = date("H",strtotime(str_replace("/","-",$_POST['fecha'])));
		$horario = $horario<8 || $horario >18?1:0;
		$venta['recargo_nocturno'] = $horario;
		$horario = date("H",strtotime(str_replace("/","-",$_POST['fecha'])));
		$horario = $horario<8 || $horario >18?'nocturno':'diurno';
		$venta['recargo_nocturno'] = $horario;
		$venta['lugar'] = $_POST['lugar'];
	    	$this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('ventas',array('venta'=>(object)$venta),TRUE),'menu'=>'solicitar_servicio','title'=>'Solicitar Servicios'));
	    }
	    else{
		$_SESSION['msj'] = $this->form_validation->error_string();
		header("Location:".base_url('cliente/servicios'));
	    }
        }
        
        function logos(){
            $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('logos',array(),TRUE)));
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
